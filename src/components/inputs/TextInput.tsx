import { FC, InputHTMLAttributes, ReactNode } from 'react';

import classNames from 'classnames';

interface Props extends InputHTMLAttributes<HTMLInputElement> {
  icon?: ReactNode;
}

const TextInput: FC<Props> = ({ icon, className, ...props }) => {
  return (
    <div className="input-container">
      {icon}
      <input
        {...props}
        className={classNames(`input ${className}`, {
          'pl-4': icon,
        })}
      />
    </div>
  );
};

export default TextInput;
