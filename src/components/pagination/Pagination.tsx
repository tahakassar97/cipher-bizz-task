import { FC } from 'react';
import { useCustomSearchParams } from '../../hooks';
import { Button } from '../buttons';

interface PaginationProps {
  totalItems: number;
  itemsPerPage: number;
  handlePageClick: (page: number) => void;
  handlePreviousClick: VoidFunction;
  handleNextClick: VoidFunction;
}

const Pagination: FC<PaginationProps> = ({
  totalItems,
  itemsPerPage,
  handlePageClick,
  handleNextClick,
  handlePreviousClick,
}) => {
  const totalPages = Math.ceil(totalItems / itemsPerPage);
  const { search } = useCustomSearchParams();

  const currentPage = +search['page'];

  const renderPagination = () => {
    const pages = [];
    for (let page = 1; page <= totalPages; page++) {
      pages.push(
        <Button
          key={page}
          onClick={() => handlePageClick(page)}
          className={`page ${
            currentPage === page || (!currentPage && page === 1) ? 'active' : ''
          }`}
        >
          {page}
        </Button>
      );
    }
    return pages;
  };

  return (
    <div className="pagination-container">
      <Button
        className={`page ${currentPage === 1 ? 'disabled' : ''}`}
        disabled={currentPage === 1}
        onClick={handlePreviousClick}
      >
        Previous
      </Button>
      {renderPagination()}
      <Button
        className={`page ${currentPage === totalPages ? 'disabled' : ''}`}
        disabled={currentPage === totalPages}
        onClick={handleNextClick}
      >
        Next
      </Button>
    </div>
  );
};

export default Pagination;
