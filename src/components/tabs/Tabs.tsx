import {
  FC,
  Children,
  ReactNode,
  isValidElement,
  cloneElement,
  useState,
  useCallback,
} from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import classNames from 'classnames';

interface TabProps {
  title: string;
  children: ReactNode;
  index?: number;
  isActive?: boolean;
  className?: string;
  handleTab?: (index: number, tabName: string) => void;
  onCustomClick?: () => void;
}

const Tab: FC<TabProps> = ({
  title,
  index,
  isActive = true,
  className = '',
  handleTab,
  onCustomClick,
}) => {
  return (
    <li
      className={classNames(`tab ${className}`, {
        'active-tab': isActive,
      })}
      onClick={() => {
        handleTab?.(index || 1, title);
        onCustomClick?.();
      }}
    >
      {title}
    </li>
  );
};

interface TabsProps {
  children: ReactNode;
  className?: string;
  syncWithUrl?: boolean;
  activeTab?: number;
  onTabChange?: (currentTabIndex: number) => void;
}

const Tabs: FC<TabsProps> = ({
  children,
  className = '',
  syncWithUrl = true,
  activeTab = 1,
  onTabChange,
}) => {
  const { hash } = useLocation();
  const { pathname } = useLocation();

  const navigate = useNavigate();

  const [tabIndex, setTabIndex] = useState(activeTab);

  const getTabName = useCallback((tabName: string) => {
    return tabName.split(' ').join('_');
  }, []);

  const getIsActive = (index: number, tabName: string) => {
    if (syncWithUrl) {
      if (hash) return hash === `#${tabName}`;

      return index === 0;
    }

    return index + 1 === tabIndex;
  };

  const handleTab = useCallback(
    (index: number, tabName: string) => {
      if (syncWithUrl) {
        const name = getTabName(tabName);

        navigate(`${pathname.slice(0, pathname.indexOf('#') + 1)}#${name}`, {
          replace: true,
        });
      }

      onTabChange?.(index);

      if (!syncWithUrl) {
        setTabIndex(index);
      }
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [syncWithUrl]
  );

  return (
    <div className="tabs-container">
      <ul className={`tabs ${className}`}>
        {Children.map(children, (child, index) => {
          if (!isValidElement(child)) return null;

          return (
            <>
              {cloneElement(child, {
                ...child.props,
                isActive: getIsActive(index, getTabName(child.props.title)),
                index: index + 1,
                handleTab,
              })}
            </>
          );
        })}
      </ul>

      {Children.map(children, (child, index) => {
        if (!isValidElement(child)) return null;

        return (
          <>
            {isValidElement(child.props.children) &&
              getIsActive(index, getTabName(child.props.title)) &&
              cloneElement(child.props.children)}
          </>
        );
      })}
    </div>
  );
};

export { Tabs, Tab };
