import { CSSProperties, FC, ReactNode } from 'react';

import Search from './Search';
import ChevronDown from './ChevronDown';
import Trash from './Trash';
import Edit from './Edit';
import Download from './Download';
import Eye from './Eye';
import Plus from './Plus';
import Check from './Check';
import Info from './Info';
import Logout from './Logout';
import File from './File';

export interface Props {
  name: Icons;
  className?: string;
  width?: string;
  height?: string;
  styles?: CSSProperties;
  viewBox?: [number, number];
  onClick?: () => void;
}

type Icons =
  | 'search'
  | 'chevron-down'
  | 'trash'
  | 'edit'
  | 'download'
  | 'eye'
  | 'plus'
  | 'info'
  | 'check'
  | 'logout'
  | 'file';

const iconsMap: { [key in Icons]: ReactNode } = {
  search: <Search />,
  'chevron-down': <ChevronDown />,
  trash: <Trash />,
  edit: <Edit />,
  download: <Download />,
  eye: <Eye />,
  plus: <Plus />,
  check: <Check />,
  info: <Info />,
  logout: <Logout />,
  file: <File />,
};

const Base: FC<Props> = ({
  name,
  className,
  height,
  width,
  styles,
  viewBox,
  onClick,
}) => {
  const Icon = iconsMap[name];

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="currentColor"
      width={width || '25'}
      height={height || '25'}
      viewBox={`0 0 ${viewBox ? viewBox[0] : width || '25'} ${
        viewBox ? viewBox[1] : height || '25'
      }`}
      className={className}
      style={styles}
      onClick={onClick}
    >
      {Icon}
    </svg>
  );
};

export default Base;
