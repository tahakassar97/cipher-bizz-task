import { FC } from 'react';

import { Button } from '../buttons';
import { Icons } from '../icons';
import { Person } from '../../interfaces';
import { Image } from '..';

interface Props {
  person: Person;
  documentsCount: number;
  onDownload: (item: Person) => void;
  onView: (item: Person) => void;
}

const Card: FC<Props> = ({
  person: { id, imageUrl, name, position, isActive },
  documentsCount,
  onDownload,
  onView,
}) => {
  return (
    <li className="card">
      <div className="card-details">
        <Image
          src={imageUrl}
          className="img"
          alt={name}
          width={80}
          height={80}
        />
        <span className="info">
          <div className="id">Staff Id: {id}</div>
          <div className="name">{name}</div>
          <div className="position">{position}</div>
        </span>
      </div>

      <div className="card-footer">
        <p>Documents ({documentsCount})</p>

        <span className="actions">
          <Button className="btn">
            <Icons name="trash" className="trash" width="16" height="16" />
          </Button>
          <Button className="btn">
            <Icons name="edit" className="edit" width="20" height="20" />
          </Button>
          <Button
            className="btn"
            onClick={() =>
              onDownload({ id, imageUrl, name, position, isActive })
            }
          >
            <Icons name="download" width="16" height="16" />
          </Button>
          <Button
            className="btn"
            onClick={() => onView({ id, imageUrl, name, position, isActive })}
          >
            <Icons name="eye" className="view" width="16" height="16" />
          </Button>
        </span>
      </div>
    </li>
  );
};

export default Card;
