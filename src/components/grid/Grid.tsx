import { FC, ReactNode } from 'react';

interface Props {
  children: ReactNode;
  className?: string;
}

const Grid: FC<Props> = ({ children, className }) => {
  return <ul className={className}>{children}</ul>;
};

export default Grid;
