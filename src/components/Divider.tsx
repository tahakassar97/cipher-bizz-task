import { FC } from 'react';

interface Props {
  className?: string;
}

const Divider: FC<Props> = ({ className }) => {
  return <span className={`divider ${className}`}></span>;
};

export default Divider;
