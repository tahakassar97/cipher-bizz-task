import { Children, useCallback, useMemo, useState } from 'react';

import {
  Grid,
  Card,
  Icons,
  Input,
  Tab,
  Tabs,
  Pagination,
  Divider,
  Button,
  Image,
} from './components';
import {
  ITEMS_PER_PAGE,
  TOTAL_ROWS,
  generatePersons,
  positions,
  status,
} from './constants';
import { useCustomSearchParams, useDropdown, useModal } from './hooks';
import { Person } from './interfaces';

const data = generatePersons();

function App() {
  const [persons, setPersons] = useState<Person[]>(
    data.slice(0, ITEMS_PER_PAGE)
  );
  const [selectedPerson, setSelectedPerson] = useState<Person | null>(null);

  const { search, setSearch } = useCustomSearchParams();

  const [
    AllDepartmentsDropdown,
    AllDepartmentsButton,
    { selectedValue: selectedDepartment },
  ] = useDropdown({
    label: 'All Departments',
    value: '',
  });

  const [
    AllStatusDropdown,
    AllStatusButton,
    { selectedValue: selectedStatus },
  ] = useDropdown({
    label: 'All Status',
    value: '',
  });

  const [ViewPersonModal, { open, close: closeModal }] = useModal();

  const totalItems = useMemo(() => {
    return selectedDepartment.value || selectedStatus.value
      ? persons.length
      : TOTAL_ROWS;
  }, [persons.length, selectedDepartment.value, selectedStatus.value]);

  const onDownload = useCallback((data: Person) => {
    const jsonData = JSON.stringify(data);

    const blob = new Blob([jsonData], { type: 'application/json' });

    const url = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.href = url;
    a.download = `${data.name}-data`;

    a.click();

    URL.revokeObjectURL(url);
  }, []);

  const onView = useCallback(
    (data: Person) => {
      open();
      setSelectedPerson(data);
    },
    [open]
  );

  const onSearch = useCallback((name: string) => {
    if (name.length === 0) {
      setPersons(data);
      return;
    }

    if (name.length < 3) return;

    const filteredData = data.filter((record) =>
      record.name.toLocaleLowerCase().includes(name.toLocaleLowerCase())
    );

    setPersons(filteredData);
  }, []);

  const onFilterByDepartment = useCallback(
    (value: string) => {
      if (!value) {
        setPersons(data);
        return;
      }

      setSearch({ page: 1 });

      const filteredData = data.filter((record) =>
        record.position.includes(value)
      );

      setPersons(filteredData);
    },
    [setSearch]
  );

  const onFilterByStatus = useCallback(
    (value: unknown) => {
      if (!value) {
        setPersons(data);
        return;
      }

      setSearch({ page: 1 });

      const filteredData = data.filter(
        (record) => record.isActive.toLowerCase() === `${value}`.toLowerCase()
      );

      setPersons(filteredData);
    },
    [setSearch]
  );

  const handlePageClick = useCallback(
    (page: number) => {
      if (selectedDepartment.value || selectedStatus.value) return;

      setSearch({ page });

      const offset = (page - 1) * ITEMS_PER_PAGE;

      const _persons = data.slice(offset, offset + ITEMS_PER_PAGE);

      setPersons(_persons);
    },
    [selectedDepartment.value, selectedStatus.value, setSearch]
  );

  const handlePreviousClick = useCallback(() => {
    if (selectedDepartment.value || selectedStatus.value) return;

    const currentPage = +search['page'];

    if (currentPage > 1) {
      const prevPage = currentPage - 1;
      setSearch({ page: prevPage });

      const limit = prevPage * ITEMS_PER_PAGE;
      const offset = (currentPage - 2) * ITEMS_PER_PAGE;

      const _persons = data.slice(offset, limit);
      setPersons(_persons);
    }
  }, [search, selectedDepartment.value, selectedStatus.value, setSearch]);

  const handleNextClick = useCallback(() => {
    if (selectedDepartment.value || selectedStatus.value) return;

    const currentPage = +search['page'] || 1;

    if (currentPage < Math.ceil(TOTAL_ROWS / ITEMS_PER_PAGE)) {
      const nextPage = currentPage + 1;
      setSearch({ page: nextPage });

      const limit = nextPage * ITEMS_PER_PAGE;
      const offset = currentPage * ITEMS_PER_PAGE;

      const _persons = data.slice(offset, limit);
      setPersons(_persons);
    }
  }, [search, selectedDepartment.value, selectedStatus.value, setSearch]);

  return (
    <>
      <ViewPersonModal>
        <div className="view-modal-container">
          <div className="body">
            <Image
              src={selectedPerson?.imageUrl}
              className="img"
              alt={selectedPerson?.name}
              width={120}
              height={120}
            />

            <section className="details">
              <h2>{selectedPerson?.name}</h2>
              <h3>{selectedPerson?.position}</h3>
              {selectedPerson?.isActive === 'active' ? (
                <Icons name="check" className="check" width="20" height="20" />
              ) : (
                <Icons name="info" className="info" width="20" height="20" />
              )}
            </section>
          </div>

          <div className="footer">
            <Button className="close-btn" onClick={closeModal}>
              Close
            </Button>
          </div>
        </div>
      </ViewPersonModal>

      <Tabs className="header">
        <Tab title="Employee Documents">
          <main className="container">
            <div className="filter-section">
              <span className="search">
                <Input
                  placeholder="Search Here..."
                  icon={<Icons name="search" className="icon" />}
                  onChange={({ target: { value } }) => onSearch(value)}
                />
              </span>

              <span className="filters">
                <div className="relative">
                  <AllDepartmentsButton />
                  <AllDepartmentsDropdown
                    values={positions}
                    onClick={(item) =>
                      onFilterByDepartment(item.value as string)
                    }
                  />
                </div>

                <div className="relative">
                  <AllStatusButton />
                  <AllStatusDropdown
                    values={status}
                    onClick={(item) => onFilterByStatus(item.value)}
                  />
                </div>

                <Button className="dropdown-btn">
                  Export All
                  <Divider />
                  <Icons name="logout" width="18" height="18" />
                </Button>

                <Button className="bulk-import-btn">
                  <Icons name="file" width="18" height="18" />
                  Bulk Import
                </Button>

                <Button className="new-document-btn">
                  <Icons name="plus" width="16" height="16" />
                  Add Documents
                </Button>
              </span>
            </div>

            <div>
              <Grid className="grid">
                {Children.toArray(
                  persons.map((person) => (
                    <Card
                      documentsCount={TOTAL_ROWS}
                      person={person}
                      onDownload={onDownload}
                      onView={onView}
                    />
                  ))
                )}
              </Grid>

              <Pagination
                itemsPerPage={ITEMS_PER_PAGE}
                totalItems={totalItems}
                handlePageClick={handlePageClick}
                handlePreviousClick={handlePreviousClick}
                handleNextClick={handleNextClick}
              />
            </div>
          </main>
        </Tab>
        <Tab title="Company Documents">Company Documents</Tab>
      </Tabs>
    </>
  );
}

export default App;
