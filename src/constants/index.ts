import { ActiveTypes } from '../interfaces';

export const positions = [
  { label: 'Frontend Developer', value: 'Frontend Developer' },
  { label: 'Backend Developer', value: 'Backend Developer' },
  { label: 'UI Designer', value: 'UI Designer' },
];

export const status = [
  { label: 'Is Active', value: 'active' },
  { label: 'Is Inactive', value: 'inactive' },
  { label: 'Pending', value: 'pending' },
];

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const getRandomName = (nameArray: any[]) => {
  const randomIndex = Math.floor(Math.random() * nameArray.length);
  return nameArray[randomIndex]?.value || nameArray[randomIndex];
};

export function generatePersons() {
  const arrayOfObjects = [];
  const firstNames = [
    'Mark',
    'Jonny',
    'Sarah',
    'Emma',
    'Michael',
    'Olivia',
    'John',
    'Sophia',
    'David',
    'Ava',
  ];
  const lastNames = [
    'Smith',
    'Johnson',
    'Brown',
    'Davis',
    'Lee',
    'Wilson',
    'Clark',
    'White',
    'Hall',
    'Jones',
  ];

  for (let i = 0; i < 60; i++) {
    const id = `ID${i + 1}`;
    const firstName = getRandomName(firstNames);
    const lastName = getRandomName(lastNames);
    const position = positions[i % positions.length].value;
    const imageUrl = `https://picsum.photos/200/200/?random=${i + 1}`;
    const documentsCount = 60;

    arrayOfObjects.push({
      id,
      name: `${firstName} ${lastName}`,
      position,
      imageUrl,
      documentsCount,
      isActive: getRandomName(status) as ActiveTypes,
    });
  }

  return arrayOfObjects;
}

export const ITEMS_PER_PAGE = 20;
export const TOTAL_ROWS = 60;
