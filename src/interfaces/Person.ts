export type ActiveTypes = 'pending' | 'active' | 'inactive'

export interface Person {
  id: string;
  name: string;
  position: string;
  imageUrl: string;
  isActive: ActiveTypes;
}
