export interface DropdownItem {
  label: string;
  value: unknown;
}
