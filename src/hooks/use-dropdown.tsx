import {
  ElementType,
  FC,
  MutableRefObject,
  ReactNode,
  useCallback,
  useRef,
  useState,
} from 'react';

import classNames from 'classnames';

import { useClickOutside } from './use-click-outside';
import { DropdownItem } from '../interfaces';
import { Button } from '../components/buttons';
import { Icons } from '../components/icons';

import './use-dropdown.css';
import { Divider } from '../components';

interface DropdownResult {
  open: () => void;
  close: () => void;
  toggle: () => void;
  isOpen: boolean;
  selectedValue: DropdownItem;
  ref: MutableRefObject<HTMLDivElement> | MutableRefObject<HTMLButtonElement>;
}

interface DropdownWrapperProps {
  className?: string;
  values?: DropdownItem[];
  onClick?: (item: DropdownItem) => void;
}

interface DropDownButtonProps {
  className?: string;
  children?: ReactNode;
}

interface DropDownItemProps extends DropdownItem {
  className?: string;
  children?: ReactNode;
  onClick?: (item: DropdownItem) => void;
}

const useDropdown = (
  defaultValue = {
    label: '',
    value: '',
  }
): [
  ElementType<DropdownWrapperProps>,
  ElementType<DropDownButtonProps>,
  DropdownResult
] => {
  const ref = useRef() as MutableRefObject<HTMLDivElement>;

  const [isOpen, setOpen] = useState(false);

  const [selectedValue, setValue] = useState<DropdownItem>(
    defaultValue.label ? defaultValue : ({} as DropdownItem)
  );

  const open = useCallback(() => {
    setOpen(true);
  }, []);

  const close = useCallback(() => {
    setOpen(false);
  }, []);

  const toggle = useCallback(() => {
    setOpen((prevState) => !prevState);
  }, []);

  useClickOutside(ref, close);

  const DropdownWrapper: FC<DropdownWrapperProps> = useCallback(
    ({ values, className = '', onClick }) => {
      return (
        <div
          className={classNames(`dropdown-wrapper ${className}`, {
            'open-dropdown': isOpen,
            hidden: !isOpen,
          })}
          ref={ref}
        >
          <ul className="dropdown-list">
            {values
              ? values?.map((item, index) => (
                  <DropdownItem
                    key={index}
                    label={item.label}
                    value={item.value}
                    className={classNames('dropdown-item', {
                      'selected-dropdown-item':
                        item.value === selectedValue.value,
                    })}
                    onClick={onClick}
                  />
                ))
              : null}

            <Button
              className="dropdown-btn w-full"
              onClick={() => {
                onClick?.({
                  label: '',
                  value: '',
                });

                close();

                setValue({
                  label: '',
                  value: '',
                });
              }}
            >
              Reset
            </Button>
          </ul>
        </div>
      );
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [isOpen, selectedValue.value]
  );

  const DropDownButton: FC<DropDownButtonProps> = useCallback(
    ({ className = '', children }) => {
      return (
        <div className="relative">
          <Button className={`dropdown-btn ${className}`} onClick={toggle}>
            {selectedValue.label || 'Select'}

            <Divider />

            <Icons name="chevron-down" width="12" height="11" />
            {children}
          </Button>
        </div>
      );
    },

    // eslint-disable-next-line react-hooks/exhaustive-deps
    [selectedValue.value, defaultValue.value]
  );

  const DropdownItem: FC<DropDownItemProps> = ({
    className,
    label,
    value,
    children,
    onClick,
  }) => {
    return (
      <li
        className={`dropdown-item ${className}`}
        onClick={(e) => {
          e.stopPropagation();
          toggle();
          onClick?.({ value, label }) || setValue({ value, label });
        }}
      >
        {children}
        {label}
      </li>
    );
  };

  return [
    DropdownWrapper,
    DropDownButton,
    { open, close, toggle, isOpen, ref, selectedValue },
  ];
};

export { useDropdown };
