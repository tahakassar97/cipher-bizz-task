import { useSearchParams, useLocation, useNavigate } from 'react-router-dom';

import queryString from 'query-string';

export const useCustomSearchParams = () => {
  const [search, setSearchParams] = useSearchParams();
  const { hash } = useLocation();
  const navigate = useNavigate();

  const searchAsObject = Object.fromEntries(new URLSearchParams(search));

  const setSearch = ({ ...param }, replace = false) => {
    const newSearch = { ...searchAsObject, ...param };
    setSearchParams(newSearch, { replace });

    navigate(
      {
        hash,
        search: queryString.stringify(newSearch),
      },
      {
        replace: true,
      }
    );
  };

  const reset = () =>
    setSearchParams('', {
      replace: true,
    });

  return {
    search: searchAsObject,
    setSearch,
    reset,
  };
};
